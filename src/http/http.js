import axios from 'axios';
import qs from 'qs';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.baseURL = '';

const fetch = (options) => {
    const { method = 'get', data, url } = options;
    switch (method.toLowerCase()) {
        case 'get':
            const uri = `${url}${options.data ? `?${qs.stringify(options.data)}` : ''}`;
            console.log('uri=', uri);
            return axios.get(uri);
        case 'post':
            return axios.post(url, data);
        default:
            return axios(options);
    }
}

export default async function http(options) {
    return fetch(options)
        .then((response) => {
            return response;
        }).catch((error) => {
            return error;
        });
}