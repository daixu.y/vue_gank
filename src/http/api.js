import config from '@/http/config';
import http from '@/http/http';
export default {
    loadTodayInfo: async function (payload) {
        let res = await http({
            url: config.LOAD_TODAY,
            method: 'get',
            data: {
                token: "123"
            }
        });
        return res.data;
    },
    loadDaysInfo: async function (payload) {
        let res = await http({
            url: config.LOAD_DAY + `/${payload.date}`,
            method: 'get',
            // data: {
            //     token: payload.data
            // }
        });
        return res.data;
    },
    loadSortList: async function (payload) {
        let res = await http({
            url: config.LOAD_DATA + `/${payload.name}/${payload.size}/${payload.num}`,
            method: 'get',
        });
        return res.data;
    },
    loadGirls: async function (payload) {
        let res = await http({
            url: config.LOAD_DATA + `/福利/${payload.size}/${payload.num}`,
            method: 'get',
        });
        return res.data;
    }
}