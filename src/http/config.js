const BASE_URL = 'https://gank.io/api';

export default {
    LOAD_DAY: `${BASE_URL}/day`,
    LOAD_TODAY: `${BASE_URL}/today`,
    LOAD_DATA: `${BASE_URL}/data`,
}