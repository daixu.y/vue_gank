export function getPromiseAction(p, commit, mutationType) {
    let promise = new Promise((resolve, reject) => {
        p.then(res => {
            if (res.error === false) {
                commit(mutationType, res);
                return resolve();
            } else {
                return reject({ code: 0, msg: res.msg })
            }
        }, error => {
            return reject({ code: 0, msg: `数据加载失败${error}` });
        }).catch(error => {
            return reject({ code: 0, msg: `数据请求异常${error}` });
        })
    })
    return promise;
}

export function getPromiseActionNoMutations(p) {
    let promise = new Promise((resolve, reject) => {
        p.then(res => {
            if (res.error === false) {
                return resolve();
            } else {
                return reject({ code: 0, msg: res.msg })
            }
        }, error => {
            return reject({ code: 0, msg: `数据加载失败${error}` });
        }).catch(error => {
            return reject({ code: 0, msg: `数据请求异常${error}` });
        })
    })
    return promise;
}