import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations'
import actions from './action'
import getters from './getters'

Vue.use(Vuex);

const state = {
    selected: "tab_news",
    todayInfo: null,
    girlsInfo: [],
    detailInfo: [],
    sortList:[],
};

const store = new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
});

export default store;