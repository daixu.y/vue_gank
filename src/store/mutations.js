import { LOAD_GIRLS, LOAD_DETAIL_INFO, LOAD_TODAY_INFO, CHANGE_TAB_SELECTED, LOAD_SORT_LIST } from './mutations-types';

export default {
    [CHANGE_TAB_SELECTED](state, data) {
        state.selected = data;
    },
    [LOAD_TODAY_INFO](state, data) {
        state.todayInfo = data;
    },
    [LOAD_GIRLS](state, data) {
        state.girlsInfo = data;
    },
    [LOAD_DETAIL_INFO](state, data) {
        state.detailInfo = data;
    },
    [LOAD_SORT_LIST](state, data) {
        state.sortList = data;
    }
}