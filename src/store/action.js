import axios from 'axios';
import api from '@/http/api';
import { LOAD_GIRLS, LOAD_DETAIL_INFO, LOAD_TODAY_INFO, LOAD_SORT_LIST } from './mutations-types';
import { getPromiseAction } from '@/utils/promiseUtils';

export default {
    async loadDayInfo({ commit }, obj) {
        const date = obj.date;
        await axios.get("day/" + date).then(response => {
            const data = response.data;
            commit(LOAD_DETAIL_INFO, data.results);
        }).catch(() => {
            console.log("error");
        });
    },
    async loadTodayInfo({ commit }) {
        await axios.get('today').then(response => {
            commit(LOAD_TODAY_INFO, response.data);
        })
    },
    loadToday({ commit, rootState }) {
        let payload = {
            token: rootState.date,
        }
        return getPromiseAction(api.loadTodayInfo(payload), commit, LOAD_TODAY_INFO);
    },
    loadDays({ commit, rootState }, payload) {
        return getPromiseAction(api.loadDaysInfo(payload), commit, LOAD_DETAIL_INFO);
    },
    loadSortList({ commit, rootState }, payload) {
        return getPromiseAction(api.loadSortList(payload), commit, LOAD_SORT_LIST);
    },
    loadGirls({ commit, rootState }, payload) {
        return getPromiseAction(api.loadGirls(payload), commit, LOAD_GIRLS);
    },
}