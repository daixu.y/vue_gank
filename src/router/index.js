import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Home from '@/components/Home'
import Detail from '@/components/Detail'
import Sort from '@/components/Sort'
import Girls from '@/components/Girls'
import Collect from '@/components/Collect'
import ImageDetail from '@/components/ImageDetail'
import SortList from '@/components/SortList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/girls',
      name: 'girls',
      component: Girls
    },
    {
      path: '/sort',
      name: 'sort',
      component: Sort
    },
    {
      path: '/collect',
      name: 'collect',
      component: Collect
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/image_detail/',
      name: 'image_detail',
      component: ImageDetail
    },
    {
      path: '/sort_list/',
      name: 'sort_list',
      component: SortList
    }
  ]
})
